﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vjezba.Web.Models.Mock
{
    public class Company
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email je obvezno unijeti (nesto@nesto.com)")]
        public string Email { get; set; }

        public string Address { get; set; }

        [Range(-90, 90)]
        public decimal Latitude { get; set; }

        [Range(-180, 180)]
        public decimal Longitude { get; set; }

        public DateTime DateFrom { get; set; }

        public int CityID { get; set; }

        public City City { get; set; }
    }

}