﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Web.Models.Mock;

namespace Vjezba.Web.Areas.Admin.Controllers
{
    [RouteArea("Admin", AreaPrefix = "Administracija")]
    [RoutePrefix("Gradovi")]
    public class CityController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            var cities = MockCityRepository.GetInstance().All()
                .ToList();

            //Naziv view-a se automatski skuzi iz naziva akcije controller-a, dovoljno je proslijediti samo model
            return View(cities);
        }

        [Route("detalji")]
        public ActionResult Details(int? id = null)
        {
            if (id == null)
                return View();

            var model = MockCityRepository.GetInstance().FindByID(id.Value);

            return View(model);
        }

        [Route("detalji/info")]
        public ActionResult DetailsPartial(int? id = null)
        {
            if (id == null)
                return null;

            var model = MockCityRepository.GetInstance().FindByID(id.Value);

            if (model == null)
                return null;

            return PartialView("_DetailsPartial", model);
        }

    }

}