﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Web.Models;
using Vjezba.Web.Models.Mock;

namespace Vjezba.Web.Controllers
{
    [RoutePrefix("komp")]
    public class CompanyController : Controller
    {
        public ActionResult Index(string query)
        {
            var companiesQuery = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(query))
                companiesQuery = companiesQuery.Where(p => p.Name.Contains(query));

            var model = companiesQuery.OrderBy(p => p.ID).ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult IndexAjax(CompanyFilterModel model)
        {
            var companiesQuery = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(model.Name))
                companiesQuery = companiesQuery.Where(p => p.Name.Contains(model.Name));

            if (!string.IsNullOrWhiteSpace(model.Address))
                companiesQuery = companiesQuery.Where(p => p.Address.Contains(model.Address));

            if (!string.IsNullOrWhiteSpace(model.Email))
                companiesQuery = companiesQuery.Where(p => p.Email.Contains(model.Email));

            if (!string.IsNullOrWhiteSpace(model.CityName))
                companiesQuery = companiesQuery.Where(p => p.City.Name.Contains(model.CityName));

            var data = companiesQuery.OrderBy(p => p.ID).ToList();

            //Iskoristit ćemo postojeći view Index. Potrebno je eksplicitno reći koji view želimo renderirati
            //jer se inače pokušava pronaći template AdvancedSearch.cshtml
            return PartialView("_IndexTable", data);
        }

        public List<SelectListItem> listCities()
        {
            var cityList = MockCityRepository.GetInstance().All().ToList();
            var cityNameList = new List<SelectListItem>();

            var listItem = new SelectListItem();
            listItem.Text = "Select";
            listItem.Value = "";
            cityNameList.Add(listItem);

            foreach (City city in cityList)
            {
                listItem = new SelectListItem();
                listItem.Text = city.Name;
                listItem.Value = city.ID.ToString();
                cityNameList.Add(listItem);
            }

            return cityNameList;
        }

        public ActionResult Create()
        {
            ViewBag.ListaGradova = listCities();

            return View();
        }

        [HttpPost]
        public ActionResult Create(Company model)
        {
            if (ModelState.IsValid)
            {
                model.ID = MockCompanyRepository.GetInstance().All()
                    .Max(p => p.ID) + 1;

                MockCompanyRepository.GetInstance().InsertOrUpdate(model);

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.ListaGradova = listCities();

                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var model = MockCompanyRepository.GetInstance().FindByID(id);
            ViewBag.ListaGradova = listCities();

            return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(int id)
        {
            var model = MockCompanyRepository.GetInstance().FindByID(id);
            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                MockCompanyRepository.GetInstance().InsertOrUpdate(model);
                return RedirectToAction("Index");
            }

            ViewBag.ListaGradova = listCities();
            return View(model);
        }


        public ActionResult Details(int? id = null)
        {
            if (id == null)
                return View();

            var model = MockCompanyRepository.GetInstance().FindByID(id.Value);

            return View(model);
        }

        [Route("pretraga/{q:alpha:length(2,5)}")]
        public ActionResult Search(string q)
        {
            //Kada se bude radilo s bazom podataka, ova metoda još neće "otići" u bazu po podatke
            var query = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(q))
                query = query.Where(p => p.Name.Contains(q));

            //Kada se bude radilo s bazom podataka, tek ova metoda bi generirala ispravni SQL i izvršila upit na bazi
            //te vratila samo potrebne rezultate
            var model = query.ToList();

            return View("Index", model);
        }


    }
}